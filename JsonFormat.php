<?php

/**
 * Created by IntelliJ IDEA.
 * User: vnapn
 * Date: 6/21/2017
 * Time: 2:49 PM
 */
class JsonFormat
{
    var $nameAPI;
    var $code;
    var $datas;

    /**
     * @param $nameAPI
     * @param $code
     * @param $datas
     * @return $this json
     */
    function formatJson($nameAPI, $code, $datas)
    {
        $this->datas = array();
        $this->nameAPI = $nameAPI;
        $this->code = $code;
        $this->datas = $datas;

        return $this;
    }
}