<?php

/**
 * Created by IntelliJ IDEA.
 * User: vnapn
 * Date: 6/21/2017
 * Time: 1:40 PM
 */
class Student
{
    public $className;
    public $name;
    public $age;
    public $address;

    function insert($className, $name, $age, $address)
    {
        $this->className = $className;
        $this->name = $name;
        $this->age = $age;
        $this->address = $address;

        return $this;
    }
}